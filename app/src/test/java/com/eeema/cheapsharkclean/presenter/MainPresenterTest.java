package com.eeema.cheapsharkclean.presenter;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.fragments.MainFragment;
import com.eeema.cheapsharkclean.app.view.MainView;
import com.eeema.cheapsharkclean.utils.FragmentTypes;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by emanuel on 7/01/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    private MainPresenter presenter;
    @Mock
    private MainView view;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        presenter = new MainPresenterImpl(view);
    }

    @Test
    public void testSelectStoresFragment(){
        when(view.selectStoresFragment()).thenReturn(MainFragment.newInstance(FragmentTypes.STORES_FRAGMENT));
        presenter.selectFragment(FragmentTypes.STORES_FRAGMENT);
        verify(view).selectStoresFragment();
    }
    @Test
    public void testSelectDealsFragment(){
        when(view.selectStoresFragment()).thenReturn(MainFragment.newInstance(FragmentTypes.DEALS_FRAGMENT));
        presenter.selectFragment(FragmentTypes.DEALS_FRAGMENT);
        verify(view).selectDealsFragment();
    }

    @Test
    public void testSelectFragmentException(){
        try{
            presenter.selectFragment(-1);
            fail("Exception not thrown");
        }catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Incorrect fragment position"));
        }
    }


    @Test
    public void testStoresSelectedTitle() throws Exception {
        when(view.getResString(R.string.stores_title_tab)).thenReturn("STORES");
        String title = presenter.getPageTitle(FragmentTypes.STORES_FRAGMENT);
        verify(view, times(1)).getResString(R.string.stores_title_tab);
        assertThat(title, is("STORES"));
    }

    @Test
    public void testDealsSelectedTitle() throws Exception {
        when(view.getResString(R.string.deals_title_tab)).thenReturn("DEALS");
        String title = presenter.getPageTitle(FragmentTypes.DEALS_FRAGMENT);
        verify(view, times(1)).getResString(R.string.deals_title_tab);
        assertThat(title, is("DEALS"));
    }

    @Test
    public void testGetTitlePageException(){
        try{
            presenter.getPageTitle(-1);
            fail("Exception not thrown");
        }catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Incorrect fragment position"));
        }
    }
}