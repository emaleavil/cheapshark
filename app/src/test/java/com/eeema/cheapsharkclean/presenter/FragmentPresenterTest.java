package com.eeema.cheapsharkclean.presenter;

import com.eeema.cheapsharkclean.app.view.FragmentView;
import com.eeema.cheapsharkclean.domain.interactors.Invoker;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Observer;

import static org.mockito.Mockito.verify;

/**
 * Created by emanuel on 7/01/16.
 */
public class FragmentPresenterTest  {

    @Mock
    FragmentView view;
    @Mock
    Invoker invoker;
    @Mock
    Observer subscriber;

    private FragmentPresenter presenter;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        presenter = new FragmentPresenterImpl(view,invoker,subscriber);
    }

    @Test
    public void testPresenterFlow(){
        presenter.getData();
        verify(invoker).execute(subscriber);
    }

}