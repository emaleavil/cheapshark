package com.eeema.cheapsharkclean.app.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.domain.model.Store;
import com.eeema.cheapsharkclean.exceptions.InvalidValueType;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by emanuel on 11/01/16.
 */
public class MockedPreferencesTest {

    @Mock
    CheapSharkApplication context;

    @Mock
    SharedPreferences sPreferences;

    @Mock
    SharedPreferences.Editor editor;

    CheapSharkPreferences preferences;



    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        setUpMocks();
        preferences = new CheapSharkPreferences(context);
    }

    public void setUpMocks(){
        when(context.getSharedPreferences(PreferenceConstants.PREFERENCES_NAME, Context.MODE_PRIVATE)).thenReturn(sPreferences);
        when(sPreferences.edit()).thenReturn(editor);

        when(sPreferences.getInt(PreferenceConstants.LOWER_PRICE,0)).thenReturn(20);
        when(sPreferences.getInt(PreferenceConstants.UPPER_PRICE,50)).thenReturn(220);
        when(sPreferences.getInt(PreferenceConstants.SORT_ORDER,0)).thenReturn(3);
        when(sPreferences.getInt(PreferenceConstants.MINIMUM_METACRITIC,50)).thenReturn(50);
        when(sPreferences.getInt(PreferenceConstants.PAGE_SIZE,60)).thenReturn(60);
        when(sPreferences.getInt(PreferenceConstants.STOREID,Integer.MIN_VALUE)).thenReturn(10);
        when(sPreferences.getBoolean(PreferenceConstants.DESC,false)).thenReturn(true);
        when(sPreferences.getBoolean(PreferenceConstants.EXACT_TITLE,false)).thenReturn(true);
        when(sPreferences.getBoolean(PreferenceConstants.RETAIL_PRICE,false)).thenReturn(false);
        when(sPreferences.getBoolean(PreferenceConstants.STEAMWORKS,false)).thenReturn(false);
        when(sPreferences.getBoolean(PreferenceConstants.ON_SALE,false)).thenReturn(true);
        when(sPreferences.getString(PreferenceConstants.TITLE,"")).thenReturn("TEST TITLE");
    }

    @Test
    public void lowerPrice(){
        int lower = preferences.getLowerPrice();
        verify(sPreferences).getInt(PreferenceConstants.LOWER_PRICE,0);
        assertThat(lower, is(20));
    }

    @Test
    public void upperPrice(){
        int upper = preferences.getUpperPrice();
        verify(sPreferences).getInt(PreferenceConstants.UPPER_PRICE,50);
        assertThat(upper, is(220));
    }

    @Test
    public void sortBy(){
        int sortBy = preferences.getSortBy();
        verify(sPreferences).getInt(PreferenceConstants.SORT_ORDER,0);
        assertThat(sortBy, is(3));
    }

    @Test
    public void minimunMetacritic(){
        int metacritic = preferences.getMinimumMetacritic();
        verify(sPreferences).getInt(PreferenceConstants.MINIMUM_METACRITIC,50);
        assertThat(metacritic, is(50));
    }

    @Test
    public void descSort(){
        boolean descSort = preferences.getDescSort();
        verify(sPreferences).getBoolean(PreferenceConstants.DESC,false);
        assertThat(descSort, is(true));
    }

    @Test
    public void exactTitle(){
        boolean exact = preferences.getExactTitle();
        verify(sPreferences).getBoolean(PreferenceConstants.EXACT_TITLE,false);
        assertThat(exact, is(true));
    }

    @Test
    public void retailPrice(){
        boolean retail = preferences.getRetailPrice();
        verify(sPreferences).getBoolean(PreferenceConstants.RETAIL_PRICE,false);
        assertThat(retail, is(false));
    }

    @Test
    public void steamOnly(){
        boolean steamOnly = preferences.getSteamOnly();
        verify(sPreferences).getBoolean(PreferenceConstants.STEAMWORKS,false);
        assertThat(steamOnly, is(false));
    }

    @Test
    public void onSale(){
        boolean retail = preferences.getOnSale();
        verify(sPreferences).getBoolean(PreferenceConstants.ON_SALE,false);
        assertThat(retail, is(true));
    }

    @Test
    public void title(){
        String title = preferences.getTitle();
        verify(sPreferences).getString(PreferenceConstants.TITLE,"");
        assertThat(title, is("TEST TITLE"));
    }

    @Test
    public void pageSize(){
        int size = preferences.getPageSize();
        verify(sPreferences).getInt(PreferenceConstants.PAGE_SIZE,60);
        assertThat(size, is(60));
    }

    @Test
    public void storeId(){
        int storeId = preferences.getStoreId();
        verify(sPreferences).getInt(PreferenceConstants.STOREID,Integer.MIN_VALUE);
        assertThat(storeId, is(10));
    }

    @Test
    public void putString() throws InvalidValueType {
        preferences.put("string", "stringPrueba");
        preferences.commit();
        verify(editor).putString("string", "stringPrueba");
        verify(editor).commit();
    }

    @Test
    public void putException(){
        Store store = new Store();
        try {
            preferences.put("storeException", store);
            fail("Exception not thrown");
        } catch (Exception exception) {
            assertThat(exception, is(instanceOf(InvalidValueType.class)));
        }
    }
}