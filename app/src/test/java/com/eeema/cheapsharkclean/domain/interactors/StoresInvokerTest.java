package com.eeema.cheapsharkclean.domain.interactors;

import com.eeema.cheapsharkclean.repository.Repository;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Subscriber;

import static org.mockito.Mockito.verify;

/**
 * Created by emanuel on 7/01/16.
 */
public class StoresInvokerTest extends TestCase {

    @Mock
    Repository repository;
    @Mock
    Subscriber subscriber;

    private StoresInvoker invoker;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        invoker = new StoresInvoker(repository);
    }

    @Test
    public void testExecute(){
        invoker.execute(subscriber);
        verify(repository).getStores(subscriber);
    }
}