package com.eeema.cheapsharkclean.datasources;

import com.eeema.cheapsharkclean.app.preferences.MapperPreferences;
import com.eeema.cheapsharkclean.repository.Repository;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Observer;

import static org.mockito.Mockito.verify;

/**
 * Created by emanuel on 7/01/16.
 */
public class RepositoryTest extends TestCase {

    @Mock
    Network restApi;
    @Mock
    MapperPreferences mapper;
    @Mock
    Observer subscriber;

    private Repository repository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        repository = new RepositoryImpl(restApi, mapper);
    }

    @Test
    public void testGetDeals() throws Exception {
        repository.getDeals(subscriber);
        verify(restApi).getDeals(mapper,subscriber);
    }



    /*@Test
    public void testGetDeal() throws Exception {
        fail("TODO");
    }*/

    @Test
    public void testGetStores() throws Exception {
        repository.getStores(subscriber);
        verify(restApi).getStores(subscriber);
    }
}