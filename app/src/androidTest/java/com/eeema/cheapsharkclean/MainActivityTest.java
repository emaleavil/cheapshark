package com.eeema.cheapsharkclean;


import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.eeema.cheapsharkclean.app.activities.MainActivity;
import com.eeema.cheapsharkclean.app.view.MainView;
import com.eeema.cheapsharkclean.app.view.SlidingTabStrip;
import com.eeema.cheapsharkclean.utils.FragmentTypes;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.eeema.cheapsharkclean.CustomMatchers.checkSelectedPage;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by emanuel on 10/12/15.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<MainActivity>(MainActivity.class);
    private MainView mainView;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Before
    public void customSetUp(){
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mainView = activityRule.getActivity();
    }

    @Test
    public void testPreconditions(){
        assertThat(activityRule, notNullValue());
        assertThat(activityRule, notNullValue());
        assertThat(activityRule, notNullValue());
    }

    @Test
    public void testViewPagerSetUp(){
        onView(ViewMatchers.withId(R.id.main_view_pager)).check(matches(notNullValue()));
    }

    @Test
    public void testMainTabs(){
        onView(withId(R.id.main_tabs)).check(matches(notNullValue()));
    }

    @Test
    public void testTabDeals(){
        onView(allOf(withParent(isAssignableFrom(SlidingTabStrip.class)), withText("DEALS"))).check(matches(notNullValue()));
    }

    @Test
    public void testTabStores(){
        onView(allOf(withParent(isAssignableFrom(SlidingTabStrip.class)), withText("STORES"))).check(matches(notNullValue()));
    }

     @Test
    public void testShowSnackBar(){
         mainView.showSnackBar("Message");
         SystemClock.sleep(500);
         onView(withText("Message")).check(matches(isDisplayed()));
    }

    @Test
    public void testChangePage() throws Throwable {
        getInstrumentation().runOnMainSync(()->{
            mainView.changePage(FragmentTypes.STORES_FRAGMENT);
        });
        onView(withId(R.id.main_view_pager)).check(matches(checkSelectedPage(FragmentTypes.STORES_FRAGMENT)));

        getInstrumentation().runOnMainSync(()->{
            mainView.changePage(FragmentTypes.DEALS_FRAGMENT);
        });
        onView(withId(R.id.main_view_pager)).check(matches(checkSelectedPage(FragmentTypes.DEALS_FRAGMENT)));
    }
}
