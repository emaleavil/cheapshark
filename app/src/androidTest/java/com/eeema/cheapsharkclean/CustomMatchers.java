package com.eeema.cheapsharkclean;

import android.support.v4.view.ViewPager;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by emanuel on 7/01/16.
 */
public class CustomMatchers {

    public static Matcher<View> checkSelectedPage(final int currentPosition){
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                if(!(item instanceof ViewPager)) return false;

                int selectedPosition = ((ViewPager) item).getCurrentItem();

                return currentPosition == selectedPosition;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Both positions haven't the same value");
            }
        };
    }
}
