package com.eeema.cheapsharkclean.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.app.adapter.PagerAdapter;
import com.eeema.cheapsharkclean.app.di.components.DaggerMainComponent;
import com.eeema.cheapsharkclean.app.di.modules.MainModule;
import com.eeema.cheapsharkclean.app.fragments.MainFragment;
import com.eeema.cheapsharkclean.app.listeners.PageChangeListener;
import com.eeema.cheapsharkclean.app.listeners.TabListener;
import com.eeema.cheapsharkclean.app.view.FragmentView;
import com.eeema.cheapsharkclean.app.view.MainView;
import com.eeema.cheapsharkclean.app.view.SlidingTabLayout;
import com.eeema.cheapsharkclean.presenter.MainPresenter;
import com.eeema.cheapsharkclean.utils.FragmentTypes;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Emanuel on 25/10/2015.
 */
public class MainActivity extends AppCompatActivity implements MainView {

    @Bind(R.id.main_view_pager)
    ViewPager viewPager;
    @Bind(R.id.main_tabs)
    SlidingTabLayout tabLayout;

    private static final String CURRENT_INDEX_KEY = "current_index";
    private int currentFragmentIndex = FragmentTypes.DEALS_FRAGMENT;

    @Inject
    MainPresenter mainPresenter;
    @Inject
    Intent filterIntent;

    TabListener tabListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeButterknife();
        initializeDagger();
        retrieveStoredFragmentIndex(savedInstanceState);
        setViewPager();

    }

    private  void initializeButterknife(){
        ButterKnife.bind(this);
    }

    private void initializeDagger(){
        DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .applicationComponent(CheapSharkApplication.getsInstance().getApplicationComponent())
                .build()
                .inject(this);
    }

    private void retrieveStoredFragmentIndex(Bundle savedInstanceState){
        currentFragmentIndex = savedInstanceState != null?  savedInstanceState.getInt(CURRENT_INDEX_KEY) : FragmentTypes.DEALS_FRAGMENT;
    }

    private void setViewPager(){
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), mainPresenter);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(currentFragmentIndex);
        viewPager.addOnPageChangeListener(new PageChangeListener(this));
        tabLayout.setCustomTabView(R.layout.custom_tab_view,android.R.id.text1);
        //making tabs to take all the space available
        tabLayout.setDistributeEvenly(true);
        tabLayout.setViewPager(viewPager);
        tabListener = new TabListener(tabLayout, this);
        tabLayout.addTabClickListener(tabListener);
    }

    @Override
    public void manageClick(int position){
        if(position == currentFragmentIndex){
           scrollUp();

        }else{
           changePage(position);
            changePageIndex(position);
        }
    }

    private void scrollUp(){
        MainFragment fragment = getVisibleFragment();
        if(fragment != null){
            fragment.scrollUp();
        }
    }

    @Override
    public void changePageIndex(int position){
        currentFragmentIndex = position;
    }

    @Override
    public void updateView() {
        FragmentView view = (FragmentView) getVisibleFragment();
        view.hideDealsView();
        view.showEmptyView();
        view.refreshData();
    }

    @Override
    public void changePage(int positionTo){
        viewPager.setCurrentItem(positionTo);
    }

    public MainFragment getVisibleFragment(){
        for (Fragment fragment : getSupportFragmentManager().getFragments()){
            if(isCurrentVisibleFragment(fragment)){
                return (MainFragment) fragment;

            }
        }
        return null;
    }

    private boolean isCurrentVisibleFragment(Fragment fragment){
        return fragment != null && fragment.isVisible() && fragment.getArguments().getInt("index") == currentFragmentIndex;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_INDEX_KEY, currentFragmentIndex);
    }

    @Override
    public Fragment selectDealsFragment() {
        return MainFragment.newInstance(FragmentTypes.DEALS_FRAGMENT);
    }

    @Override
    public Fragment selectStoresFragment(){
        return MainFragment.newInstance(FragmentTypes.STORES_FRAGMENT);
    }

    public String getResString(int resId){
        return getString(resId);
    }

    @Override
    public void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(viewPager,message,Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(filterIntent);
        return true;
    }
}
