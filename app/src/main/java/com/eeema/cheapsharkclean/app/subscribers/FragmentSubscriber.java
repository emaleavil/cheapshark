package com.eeema.cheapsharkclean.app.subscribers;

import android.support.v7.widget.RecyclerView;

import com.eeema.cheapsharkclean.app.view.FragmentView;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by emanuel on 3/12/15.
 */
public class FragmentSubscriber implements Observer<RecyclerView.Adapter> {

    private FragmentView view;

    @Inject
    public FragmentSubscriber(FragmentView view){
        this.view = view;
    }

    @Override
    public void onCompleted() {
        if (view != null){
            view.finishRefreshing();

        }

    }

    @Override
    public void onError(Throwable e) {

        if( view != null || e != null){
            view.hideDealsView();
            view.showEmptyView();
            view.showErrorMessage(e.getMessage());
            view.finishRefreshing();

        }

    }

    @Override
    public void onNext(RecyclerView.Adapter  baseAdapter) {

        if(view != null || baseAdapter != null){
            view.hideEmptyView();
            view.showDealsView();
            view.showData(baseAdapter);

        }
    }
}