package com.eeema.cheapsharkclean.app.di.components;

import com.eeema.cheapsharkclean.app.activities.MainActivity;
import com.eeema.cheapsharkclean.app.di.PerActivity;
import com.eeema.cheapsharkclean.app.di.modules.MainModule;

import dagger.Component;

/**
 * Created by emanuel on 7/12/15.
 */

@PerActivity
@Component(dependencies = {ApplicationComponent.class}, modules = {MainModule.class})
public interface MainComponent {
    void inject(MainActivity mainActivity);
}
