package com.eeema.cheapsharkclean.app.adapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.domain.model.Deal;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by V361964 on 26-10-2015.
 */
public class DealsAdapter extends RecyclerView.Adapter {

    private List<Deal> deals;
    private LayoutInflater inflater;

    public DealsAdapter(List<Deal> deals){
        this.deals = deals;
        this.inflater = LayoutInflater.from(CheapSharkApplication.getsInstance());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View parentView = inflater.inflate(R.layout.deals_item_layout,parent,false);
       return new Holder(parentView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final Deal currentDeal = deals.get(position);

        ((Holder)holder).name.setText(currentDeal.getTitle());
        ((Holder)holder).prices.setText(currentDeal.getSalePrice() + "/" + currentDeal.getNormalPrice());
        ((Holder)holder).savings.setText(String.valueOf(Math.round(Float.valueOf(currentDeal.getSavings()))) + "%");

        Glide.with(CheapSharkApplication.getsInstance()).load(currentDeal.getThumb()).asBitmap().centerCrop().into(new BitmapImageViewTarget(((Holder)holder).thumb) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(CheapSharkApplication.getsInstance().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                ((Holder)holder).thumb.setImageDrawable(circularBitmapDrawable);
            }
        });

        //Don't use butterknife method because we need current deal
        ((Holder)holder).parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               intent.setData(Uri.parse("http://www.cheapshark.com/redirect.php?dealID=" + currentDeal.getId()));
                CheapSharkApplication.getsInstance().startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return deals.size();
    }

    static class Holder  extends RecyclerView.ViewHolder{
        public View parentView;
        @Bind(R.id.name) public TextView name;
        @Bind(R.id.savings) public TextView savings;
        @Bind(R.id.prices)  public TextView prices;
        @Bind(R.id.thumb)  public ImageView thumb;

        public Holder(View itemView) {
            super(itemView);
            this.parentView = itemView;
            ButterKnife.bind(this,parentView);
        }
    }
}
