package com.eeema.cheapsharkclean.app.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.eeema.cheapsharkclean.exceptions.InvalidValueType;

import java.util.Set;

/**
 * Created by emanuel on 14/12/15.
 */
public class CheapSharkPreferences {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public CheapSharkPreferences(Context context){
        preferences = context.getSharedPreferences(PreferenceConstants.PREFERENCES_NAME, context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void put(String key, Object value) throws InvalidValueType{
        if(value == null) return;

        if(value instanceof String){
            putString(editor, key, (String) value);
        }

        if(value instanceof Integer){
            putInteger(editor, key, (Integer) value);
        }

        if(value instanceof Float){
            putFloat(editor, key, (Float) value);
        }

        if(value instanceof Boolean){
            putBoolean(editor, key, (Boolean) value);
        }

        if(value instanceof Long){
            putLong(editor, key, (Long) value);
        }


        if(isStringSet(value)){
            putStringSet(editor, key, (Set<String>)value);

        }

        if(!isValidType(value)){
            throw new InvalidValueType();
        }

    }

    private boolean isValidType(Object value){
        boolean success = false;
        success =  value instanceof String;
        success = success || value instanceof Integer;
        success = success || value instanceof Float;
        success = success || value instanceof Boolean;
        success = success || value instanceof Long;
        success = success || isStringSet(value);

        return success;
    }

    private boolean isStringSet(Object value){
        return value instanceof Set && ((Set<Object>)value).iterator().hasNext() && ((Set<Object>)value).iterator().next() instanceof String;
    }

    public  int getSortBy()  {
        return preferences.getInt(PreferenceConstants.SORT_ORDER,PreferenceConstants.DEFAULT_SORT_ORDER);

    }

    public  int getLowerPrice() {
        return preferences.getInt(PreferenceConstants.LOWER_PRICE, PreferenceConstants.DEFAULT_LOWER_PRICE);
    }

    public  int getUpperPrice() {
        return preferences.getInt(PreferenceConstants.UPPER_PRICE, PreferenceConstants.DEFAULT_UPPER_PRICE);
    }

    public  int getMinimumMetacritic() {
        return preferences.getInt(PreferenceConstants.MINIMUM_METACRITIC, PreferenceConstants.DEFAULT_MINIMUM_METACRITIC);
    }

    public boolean getDescSort(){
        return preferences.getBoolean(PreferenceConstants.DESC,false);
    }

    public boolean getExactTitle(){
        return preferences.getBoolean(PreferenceConstants.EXACT_TITLE,false);
    }

    public boolean getRetailPrice(){
        return preferences.getBoolean(PreferenceConstants.RETAIL_PRICE,false);
    }

    public boolean getSteamOnly(){
        return preferences.getBoolean(PreferenceConstants.STEAMWORKS,false);
    }

    public boolean getOnSale(){
        return preferences.getBoolean(PreferenceConstants.ON_SALE,false);
    }

    public String getTitle(){
        return preferences.getString(PreferenceConstants.TITLE, "");
    }

    @NonNull
    public Integer getPageSize(){
        return preferences.getInt(PreferenceConstants.PAGE_SIZE, PreferenceConstants.DEFAULT_PAGE_SIZE);
    }

    @NonNull
    public  Integer getStoreId(){
        return preferences.getInt(PreferenceConstants.STOREID, Integer.MIN_VALUE);
    }


    private void putString(SharedPreferences.Editor editor, String key, String value){
        editor.putString(key, value);
    }

    private void putInteger(SharedPreferences.Editor editor, String key, Integer value) {
        editor.putInt(key, value);
    }

    private void putFloat(SharedPreferences.Editor editor, String key, Float value) {
        editor.putFloat(key, value);
    }

    private void putBoolean(SharedPreferences.Editor editor, String key, Boolean value) {
        editor.putBoolean(key, value);
    }

    private void putLong(SharedPreferences.Editor editor, String key, Long value) {
        editor.putLong(key,value);
    }



    private void putStringSet(SharedPreferences.Editor editor, String key, Set<String> value) {
        editor.putStringSet(key,value);
    }

    public void commit(){
        editor.commit();
    }
    public void apply(){
        editor.apply();
    }
}