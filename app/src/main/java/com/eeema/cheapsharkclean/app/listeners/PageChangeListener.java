package com.eeema.cheapsharkclean.app.listeners;

import android.support.v4.view.ViewPager;

import com.eeema.cheapsharkclean.app.view.MainView;

/**
 * Created by emanuel on 15/12/15.
 */
public class PageChangeListener implements ViewPager.OnPageChangeListener {

    private MainView mainView;

    public PageChangeListener(MainView mainView){
        this.mainView = mainView;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mainView.changePageIndex(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
