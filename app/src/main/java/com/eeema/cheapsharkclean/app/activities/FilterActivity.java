package com.eeema.cheapsharkclean.app.activities;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.app.di.components.DaggerFilterComponent;
import com.eeema.cheapsharkclean.app.di.modules.FilterModule;
import com.eeema.cheapsharkclean.app.preferences.CheapSharkPreferences;
import com.eeema.cheapsharkclean.app.preferences.PreferenceConstants;
import com.eeema.cheapsharkclean.app.view.FilterView;
import com.eeema.cheapsharkclean.presenter.FilterPresenter;

import java.util.HashMap;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by emanuel on 14/12/15.
 */
public class FilterActivity extends AppCompatActivity implements FilterView{

    private static final String TAG = FilterActivity.class.getName();

    @Bind(R.id.spinner_sort_by)
    Spinner sortBy;
    @Bind(R.id.lower_price)
    SeekBar lowerPrice;
    @Bind(R.id.upper_price)
    SeekBar upperPrice;
    @Bind(R.id.metacritic)
    SeekBar metacritic;
    @Bind(R.id.sort_direction)
    CheckedTextView sortDirection;
    @Bind(R.id.exact)
    CheckedTextView exactTitle;
    @Bind(R.id.retail_price)
    CheckedTextView retailPrice;
    @Bind(R.id.steamworks)
    CheckedTextView steamworks;
    @Bind(R.id.onSale)
    CheckedTextView onSale;


    @Inject
    FilterPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        initializeDagger();
        initActionBar();
    }

    private void initializeDagger(){
        DaggerFilterComponent.builder()
                .filterModule(new FilterModule(this))
                .applicationComponent(CheapSharkApplication.getsInstance().getApplicationComponent())
                .build()
                .inject(this);
    }

    public void initActionBar(){
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
            upArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
            actionBar.setHomeAsUpIndicator(upArrow);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        retrievePreferencesStored();
    }

    public void retrievePreferencesStored()  {
        CheapSharkPreferences preferences = CheapSharkApplication.getsInstance().getPreferences();
        sortBy.setSelection(preferences.getSortBy(),true);
        lowerPrice.setProgress(preferences.getLowerPrice());
        upperPrice.setProgress(preferences.getUpperPrice());
        metacritic.setProgress(preferences.getMinimumMetacritic());
        sortDirection.setChecked(preferences.getDescSort());
        exactTitle.setChecked(preferences.getExactTitle());
        steamworks.setChecked(preferences.getSteamOnly());
        onSale.setChecked(preferences.getOnSale());
        retailPrice.setChecked(preferences.getRetailPrice());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            presenter.saveInPreferences(createMapValue());
            return true;
        }

         return super.onOptionsItemSelected(item);

    }

    private HashMap<String,Object> createMapValue(){
        HashMap<String,Object> mapper = new HashMap<String, Object>();

        mapper.put(PreferenceConstants.SORT_ORDER, sortBy.getSelectedItemPosition());
        mapper.put(PreferenceConstants.LOWER_PRICE, lowerPrice.getProgress());
        mapper.put(PreferenceConstants.UPPER_PRICE, upperPrice.getProgress());
        mapper.put(PreferenceConstants.MINIMUM_METACRITIC, metacritic.getProgress());
        mapper.put(PreferenceConstants.DESC, sortDirection.isChecked());
        mapper.put(PreferenceConstants.EXACT_TITLE, exactTitle.isChecked());
        mapper.put(PreferenceConstants.STEAMWORKS, steamworks.isChecked());
        mapper.put(PreferenceConstants.ON_SALE, onSale.isChecked());
        mapper.put(PreferenceConstants.RETAIL_PRICE, retailPrice.isChecked());

        return mapper;
    }

    @Override
    public void valueCannotBeStored(String valueKey) {
        Log.i(TAG,valueKey + " cannot be stored, invalid value type");
    }

    @Override
    public void commitFinished() {
        super.onBackPressed();
    }

    @OnClick(R.id.sort_direction)
    void sortDirection(View v){
        sortDirection.setChecked(!((CheckedTextView)v).isChecked());
    }

    @OnClick(R.id.exact)
    void exactTitle(View v){
        exactTitle.setChecked(!((CheckedTextView)v).isChecked());
    }

    @OnClick(R.id.steamworks)
    void steamworks(View v) {
        steamworks.setChecked(!((CheckedTextView)v).isChecked());
    }

    @OnClick(R.id.onSale)
    void onSale(View v){
        onSale.setChecked(!((CheckedTextView)v).isChecked());
    }

    @OnClick(R.id.retail_price)
    void retailPrice(View v){
        retailPrice.setChecked(!((CheckedTextView)v).isChecked());
    }

}
