package com.eeema.cheapsharkclean.app.view;

import android.support.v4.app.Fragment;

/**
 * Created by emanuel on 7/12/15.
 */
public interface MainView {

    Fragment selectDealsFragment();
    Fragment selectStoresFragment();
    String getResString(int resId);
    void showSnackBar(String message);
    void manageClick(int position);
    void changePage(int positionTo);
    void changePageIndex(int position);
    void updateView();
}
