package com.eeema.cheapsharkclean.app;

import android.app.Application;

import com.eeema.cheapsharkclean.app.di.components.ApplicationComponent;
import com.eeema.cheapsharkclean.app.di.components.DaggerApplicationComponent;
import com.eeema.cheapsharkclean.app.di.modules.ApplicationModule;
import com.eeema.cheapsharkclean.app.preferences.CheapSharkPreferences;

import javax.inject.Inject;

/**
 * Created by emanuel on 3/12/15.
 */
public class CheapSharkApplication extends Application {

    private static CheapSharkApplication sInstance = null;

    private ApplicationComponent component;

    @Inject
    CheapSharkPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        component  = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
        component.inject(sInstance);
    }

    public static CheapSharkApplication getsInstance(){
        return sInstance;
    }

    public ApplicationComponent getApplicationComponent(){
        return component;
    }

    public CheapSharkPreferences getPreferences(){
        return preferences;
    }
}
