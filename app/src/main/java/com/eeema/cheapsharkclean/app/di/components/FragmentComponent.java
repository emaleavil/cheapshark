package com.eeema.cheapsharkclean.app.di.components;

import com.eeema.cheapsharkclean.app.di.PerActivity;
import com.eeema.cheapsharkclean.app.di.modules.FragmentModule;
import com.eeema.cheapsharkclean.app.di.modules.MainModule;
import com.eeema.cheapsharkclean.app.fragments.MainFragment;

import dagger.Component;

/**
 * Created by emanuel on 7/12/15.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {MainModule.class, FragmentModule.class})
public interface FragmentComponent extends MainComponent {

    void inject(MainFragment fragment);
}
