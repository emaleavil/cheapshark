package com.eeema.cheapsharkclean.app.di.modules;

import com.eeema.cheapsharkclean.app.fragments.BaseFragment;
import com.eeema.cheapsharkclean.app.subscribers.FragmentSubscriber;
import com.eeema.cheapsharkclean.domain.interactors.DealsInvoker;
import com.eeema.cheapsharkclean.domain.interactors.Invoker;
import com.eeema.cheapsharkclean.domain.interactors.StoresInvoker;
import com.eeema.cheapsharkclean.presenter.FragmentPresenter;
import com.eeema.cheapsharkclean.presenter.FragmentPresenterImpl;
import com.eeema.cheapsharkclean.repository.Repository;
import com.eeema.cheapsharkclean.utils.FragmentTypes;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emanuel on 7/12/15.
 */
@Module
public class FragmentModule {

    private BaseFragment fragment;

    public FragmentModule(BaseFragment fragment){
        this.fragment = fragment;
    }

    @Provides
    public BaseFragment  provideView(){
        return fragment;
    }

    @Provides
    public FragmentPresenter providePresenter(Invoker invoker, FragmentSubscriber subscriber){
        return new FragmentPresenterImpl(fragment,invoker,subscriber);
    }

    @Provides
    public Invoker provideInvoker(Repository repository){
        if(fragment.getType() == FragmentTypes.STORES_FRAGMENT){
            return new StoresInvoker(repository);
        }

        return new DealsInvoker(repository);
    }

    @Provides
    public FragmentSubscriber provideSubscriber(){
        return new FragmentSubscriber(fragment);
    }
}
