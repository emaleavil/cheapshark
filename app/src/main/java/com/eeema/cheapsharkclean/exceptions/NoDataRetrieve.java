package com.eeema.cheapsharkclean.exceptions;

/**
 * Created by V361964 on 26-10-2015.
 */
public class NoDataRetrieve extends Exception {

    public NoDataRetrieve(String detailMessage) {
        super(detailMessage);
    }
}
