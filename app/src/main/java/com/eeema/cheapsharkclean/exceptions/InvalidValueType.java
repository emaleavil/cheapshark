package com.eeema.cheapsharkclean.exceptions;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.CheapSharkApplication;

/**
 * Created by emanuel on 14/12/15.
 */
public class InvalidValueType extends Exception {

    public InvalidValueType() {
    }

    @Override
    public String getMessage() {
        return CheapSharkApplication.getsInstance().getString(R.string.invalid_value_type_message);
    }
}


