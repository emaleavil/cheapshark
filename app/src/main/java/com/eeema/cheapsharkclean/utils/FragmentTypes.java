package com.eeema.cheapsharkclean.utils;

/**
 * Created by emanuel on 9/12/15.
 */
public abstract class FragmentTypes {

    public static final int DEALS_FRAGMENT = 0;
    public static final int STORES_FRAGMENT = 1;
}