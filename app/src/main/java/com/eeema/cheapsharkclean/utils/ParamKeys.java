package com.eeema.cheapsharkclean.utils;

/**
 * Created by Emanuel on 25/10/2015.
 */
public class ParamKeys {



    public abstract class Deals{

        public static final String DEAL_ID = "id";
        public static final String STORE_ID = "storeID";
        public static final String PAGE_SIZE = "pageSize";
        public static final String SORT_BY = "sortBy";
        public static final String DESC = "desc";
        public static final String LOWER_PRICE = "lowerPrice";
        public static final String UPPER_PRICE = "upperPrice";
        public static final String METACRITIC = "metacritic";
        public static final String TITLE = "title";
        public static final String EXACT = "exact";
        public static final String RETAIL_PRICE_ONLY = "AAA";
        public static final String STEAM_WORKS = "steamworks";
        public static final String ON_SALE = "onSale";

    }

    public abstract class Alert{

        public static final String ACTION = "action";
        public static final String EMAIL = "email";
        public static final String GAME_ID = "gameID";
        public static final String PRICE = "price";
    }
}
