package com.eeema.cheapsharkclean.utils;

/**
 * Created by emanuel on 29/10/15.
 */
public abstract class SortCriteria {

    public static final String DEAL_RATING = "Deal Rating";
    public static final String TITLE = "Title";
    public static final String SAVINGS = "Savings";
    public static final String PRICE = "Price";
    public static final String METACRITIC = "Metacritic";
    public static final String RELEASE = "Release";
    public static final String STORE = "Store";

}
