package com.eeema.cheapsharkclean.presenter;

import android.support.v4.app.Fragment;

import com.eeema.cheapsharkclean.R;
import com.eeema.cheapsharkclean.app.view.MainView;
import com.eeema.cheapsharkclean.utils.FragmentTypes;

import javax.inject.Inject;

/**
 * Created by emanuel on 7/12/15.
 */
public class MainPresenterImpl implements MainPresenter {

    private MainView mainView;

    @Inject
    public MainPresenterImpl(MainView mainView){
        this.mainView = mainView;
    }

    @Override
    public Fragment selectFragment(int position){
        if(position == FragmentTypes.DEALS_FRAGMENT){
            return mainView.selectDealsFragment();
        }

        if(position == FragmentTypes.STORES_FRAGMENT){
            return mainView.selectStoresFragment();
        }

        throw new IllegalArgumentException("Incorrect fragment position");
    }

    @Override
    public String getPageTitle(int position){

        if(position == FragmentTypes.DEALS_FRAGMENT){
                return mainView.getResString(R.string.deals_title_tab);
        }

        if(position == FragmentTypes.STORES_FRAGMENT){
            return mainView.getResString(R.string.stores_title_tab);
        }

        throw new IllegalArgumentException("Incorrect fragment position");
    }

    @Override
    public void showError(String message) {

    }


}
