package com.eeema.cheapsharkclean.presenter;

import com.eeema.cheapsharkclean.app.view.FragmentView;
import com.eeema.cheapsharkclean.domain.interactors.Invoker;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by emanuel on 9/12/15.
 */
public class FragmentPresenterImpl implements FragmentPresenter {

    private FragmentView view;
    private Invoker invoker;
    private Observer subscriber;

   @Inject
    public FragmentPresenterImpl(FragmentView view, Invoker invoker, Observer subscriber){
       this.view = view;
       this.invoker = invoker;
       this.subscriber = subscriber;
    }

    @Override
    public void getData() {
        invoker.execute(subscriber);
    }
}
