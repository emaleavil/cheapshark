package com.eeema.cheapsharkclean.presenter;


import com.eeema.cheapsharkclean.app.CheapSharkApplication;
import com.eeema.cheapsharkclean.app.preferences.CheapSharkPreferences;
import com.eeema.cheapsharkclean.app.view.FilterView;
import com.eeema.cheapsharkclean.exceptions.InvalidValueType;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by emanuel on 14/12/15.
 */
public class FilterPresenterImpl implements FilterPresenter {

    private FilterView view;

    @Inject
    public FilterPresenterImpl(FilterView view){
        this.view = view;
    }


    @Override
    public void saveInPreferences(HashMap<String,Object> values) {
        CheapSharkPreferences preferences =CheapSharkApplication.getsInstance().getPreferences();
        for(Map.Entry<String,Object> row : values.entrySet()){

            try {
                preferences.put(row.getKey(),row.getValue());

            } catch (InvalidValueType invalidValueType) {
                //Value cannot be stored
                view.valueCannotBeStored(row.getKey());
            }
        }

        preferences.commit();
        view.commitFinished();
    }

}
