package com.eeema.cheapsharkclean.presenter;

/**
 * Created by emanuel on 9/12/15.
 */
public interface FragmentPresenter {
    void getData();
}
