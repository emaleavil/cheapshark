package com.eeema.cheapsharkclean.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by emanuel on 25/10/15.
 */
public class Store {

    @SerializedName("storeID")
    private String storeID;
    @SerializedName("storeName")
    private String storeName;

    public String getStoreID() {
        return storeID;
    }

    public String getStoreName() {
        return storeName;
    }
}
