package com.eeema.cheapsharkclean.domain.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by emanuel on 25/10/15.
 */
public class Game {

    @SerializedName("gameID")
    private String id;
    @SerializedName("steamAppID")
    private String steamAppID;
    @SerializedName("cheapest")
    private String cheapestPrice;
    @SerializedName("cheapestDealID")
    private String cheapestDealID;
    @SerializedName("external")
    private String external;
    @SerializedName("thumb")
    private String thumb;

    public String getId() {
        return id;
    }

    public String getSteamAppID() {
        return steamAppID;
    }

    public String getCheapestPrice() {
        return cheapestPrice;
    }

    public String getCheapestDealID() {
        return cheapestDealID;
    }

    public String getExternal() {
        return external;
    }

    public String getThumb() {
        return thumb;
    }
}
