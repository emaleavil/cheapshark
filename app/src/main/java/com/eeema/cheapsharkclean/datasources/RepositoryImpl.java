package com.eeema.cheapsharkclean.datasources;

import com.eeema.cheapsharkclean.app.preferences.MapperPreferences;
import com.eeema.cheapsharkclean.repository.Repository;

import javax.inject.Inject;

import rx.Observer;

/**
 * Created by V361964 on 26-10-2015.
 */
public class RepositoryImpl implements Repository {

    private Network restApi;
    private MapperPreferences mapper;
    //TODO add database

    @Inject
    public RepositoryImpl(Network restApi, MapperPreferences mapper){
        this.mapper = mapper;
        this.restApi = restApi;
    }

    @Override
    public void getDeals(Observer subscriber) {
        restApi.getDeals(mapper, subscriber);
    }

    @Override
    public void getDeal(String dealID) {

    }

    @Override
    public void getStores(Observer subscriber) {
        restApi.getStores(subscriber);
    }
}
