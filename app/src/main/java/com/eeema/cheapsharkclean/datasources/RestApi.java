package com.eeema.cheapsharkclean.datasources;

import com.eeema.cheapsharkclean.domain.model.Deal;
import com.eeema.cheapsharkclean.domain.model.Store;
import com.eeema.cheapsharkclean.utils.ParamKeys;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Emanuel on 25/10/2015.
 */
public interface RestApi {

    /*WITHOUT STOREID AND TITLE*/
    @GET("deals")
    Observable<List<Deal>> getDeals(
                              @Query(ParamKeys.Deals.PAGE_SIZE) int pageSize,
                              @Query(ParamKeys.Deals.SORT_BY) String sortBy,
                              @Query(ParamKeys.Deals.DESC)int desc,@Query(ParamKeys.Deals.LOWER_PRICE)int lowerPrice,
                              @Query(ParamKeys.Deals.UPPER_PRICE)int upperPrice, @Query(ParamKeys.Deals.METACRITIC) int metacritic,
                              @Query(ParamKeys.Deals.RETAIL_PRICE_ONLY)int retail,
                              @Query(ParamKeys.Deals.STEAM_WORKS)int steamworks,@Query(ParamKeys.Deals.ON_SALE)int onSale);

    /*WITH STOREID AND WITHOUT TITLE*/
    @GET("deals")
    Observable<List<Deal>> getDeals(@Query(ParamKeys.Deals.STORE_ID)int storeId,
                              @Query(ParamKeys.Deals.PAGE_SIZE) int pageSize,
                              @Query(ParamKeys.Deals.SORT_BY) String sortBy,
                              @Query(ParamKeys.Deals.DESC)int desc,@Query(ParamKeys.Deals.LOWER_PRICE)int lowerPrice,
                              @Query(ParamKeys.Deals.UPPER_PRICE)int upperPrice, @Query(ParamKeys.Deals.METACRITIC) int metacritic,
                              @Query(ParamKeys.Deals.RETAIL_PRICE_ONLY)int retail,
                              @Query(ParamKeys.Deals.STEAM_WORKS)int steamworks,@Query(ParamKeys.Deals.ON_SALE)int onSale);

    @GET("deals")
    Observable<List<Deal>> getDeals(
                              @Query(ParamKeys.Deals.PAGE_SIZE) int pageSize,
                              @Query(ParamKeys.Deals.SORT_BY) String sortBy,
                              @Query(ParamKeys.Deals.DESC)int desc,@Query(ParamKeys.Deals.LOWER_PRICE)int lowerPrice,
                              @Query(ParamKeys.Deals.UPPER_PRICE)int upperPrice, @Query(ParamKeys.Deals.METACRITIC) int metacritic,
                              @Query(ParamKeys.Deals.TITLE)String title,@Query(ParamKeys.Deals.EXACT)int exact,@Query(ParamKeys.Deals.RETAIL_PRICE_ONLY)int retail,
                              @Query(ParamKeys.Deals.STEAM_WORKS)int steamworks,@Query(ParamKeys.Deals.ON_SALE)int onSale);

    /*WITHOUT STOREID AND WITH TITLE*/
    @GET("deals")
    Observable<Deal> getDeal(@Query(ParamKeys.Deals.DEAL_ID) String dealId);

    @GET("stores")
    Observable<List<Store>> getStores();

    @GET("alerts")
    Observable<Boolean> alert(@Query(ParamKeys.Alert.ACTION)String action,
                        @Query(ParamKeys.Alert.EMAIL)String email,
                        @Query(ParamKeys.Alert.GAME_ID)int gameId,
                        @Query(ParamKeys.Alert.PRICE) float price);

}
