package com.eeema.cheapsharkclean.app.di.modules;

import android.content.Context;

import com.eeema.cheapsharkclean.app.preferences.CheapSharkPreferences;
import com.eeema.cheapsharkclean.app.preferences.MapperPreferences;
import com.eeema.cheapsharkclean.datasources.Network;
import com.eeema.cheapsharkclean.datasources.NetworkImpl;
import com.eeema.cheapsharkclean.datasources.RepositoryImpl;
import com.eeema.cheapsharkclean.repository.Repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emanuel on 7/12/15.
 */

@Singleton
@Module
public class ApplicationModule {

    private Context appContext;

    public ApplicationModule(Context appContext){
        this.appContext = appContext;
    }

    @Provides @Singleton
    public Context provideAppContext(){
       return appContext;
    }

    @Provides @Singleton
    Repository provideRepository(Network network, MapperPreferences mapper){
        return new RepositoryImpl(network,mapper);
    }

    @Provides @Singleton
    Network provideNetwork(){
        return new NetworkImpl();
    }

    @Provides @Singleton
    MapperPreferences provideMapper(){
        return new MapperPreferences();
    }

    @Provides @Singleton
    CheapSharkPreferences providePreferences(){return new CheapSharkPreferences(appContext);}
}
