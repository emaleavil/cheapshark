package com.eeema.cheapsharkkotlin.presenter

import android.support.v4.app.Fragment

/**
 * Created by emanuel on 14/01/16.
 */
interface MainPresenter {

    fun selectFragment(position : Int) : Fragment ;
    fun getPageTitle(position : Int) : String;
    fun showError(message : String);

}