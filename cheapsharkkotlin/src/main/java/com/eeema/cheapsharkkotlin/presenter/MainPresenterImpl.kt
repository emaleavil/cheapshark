package com.eeema.cheapsharkkotlin.presenter

import android.support.v4.app.Fragment
import com.eeema.cheapsharkkotlin.R
import com.eeema.cheapsharkkotlin.app.view.MainView
import com.eeema.cheapsharkkotlin.utils.FragmentTypes
import javax.inject.Inject

/**
 * Created by emanuel on 14/01/16.
 */
class MainPresenterImpl public @Inject constructor (mainView : MainView) : MainPresenter {

    private var mainView : MainView = mainView;

    override fun selectFragment(position: Int): Fragment {
        if(position == FragmentTypes.DEALS_FRAGMENT){
            return mainView.selectDealsFragment();
        }

        if(position == FragmentTypes.STORES_FRAGMENT){
            return mainView.selectStoresFragment();
        }

        throw IllegalAccessException("Incorrect fragment position");
    }

    override fun getPageTitle(position: Int): String {
        if(position == FragmentTypes.DEALS_FRAGMENT){
            return mainView.getResString(R.string.deals_title_tab);
        }

        if(position == FragmentTypes.STORES_FRAGMENT){
            return mainView.getResString(R.string.stores_title_tab);
        }

        throw IllegalAccessException("Incorrect fragment position");
    }

    override fun showError(message: String) {
        //TODO create this method
        return;
    }
}