package com.eeema.cheapsharkkotlin.presenter

import android.support.v7.widget.RecyclerView
import com.eeema.cheapsharkkotlin.app.view.FragmentView
import com.eeema.cheapsharkkotlin.domain.interactors.Invoker
import rx.Observer
import javax.inject.Inject

/**
 * Created by emanuel on 14/01/16.
 */
class FragmentPresenterImpl public @Inject constructor(view: FragmentView, invoker :
Invoker<RecyclerView.Adapter<RecyclerView.ViewHolder>>, subscriber :
Observer<RecyclerView.Adapter<RecyclerView.ViewHolder> >) : FragmentPresenter{

    private var view : FragmentView = view;
    private var invoker : Invoker<RecyclerView.Adapter<RecyclerView.ViewHolder>> = invoker;
    private var subscriber : Observer<RecyclerView.Adapter<RecyclerView.ViewHolder>> = subscriber;

    override fun getData() {
        invoker.execute(subscriber)
    }

}
