package com.eeema.cheapsharkkotlin.app.view

import android.support.v4.app.Fragment

/**
 * Created by emanuel on 14/01/16.
 */
public interface MainView {

    fun selectDealsFragment() : Fragment;
    fun selectStoresFragment() : Fragment;
    fun getResString(resId : Int) : String;
    fun showSnackBar(message : String);
    fun manageClick(position : Int);
    fun changePage(positionTo : Int);
    fun changePageIndex(position : Int);
}