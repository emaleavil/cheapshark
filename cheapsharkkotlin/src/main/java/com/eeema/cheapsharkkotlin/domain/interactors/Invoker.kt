package com.eeema.cheapsharkkotlin.domain.interactors

import rx.Observer

/**
 * Created by emanuel on 14/01/16.
 */
interface Invoker<T> {
    fun execute(subscriber : Observer<T>);
}